
function sumThreeNumbers( n1=0, n2=0, n3=0 ) {
    // получает три числа и возвращает их сумму
   let sum = 0;
   sum += Number (n1) + Number (n2) + Number (n3);
  /*
   for ( let i = 1; i<4; i++ ) {
       sum += Number( prompt("Please enter " + i + " of numbers:"));
   }*/
    alert('Sum of your numbers is : ' + sum);
    return sum;
}

function sumNumbersOrder(){
    //подсчитывает сумму чисел от 1 до заданного X.
    let nMax = Number( prompt("Please enter number:"));
    let sum = 0;
    for ( let i = 1; i<=nMax; i++ ) {
        sum +=i;
    }
    alert('Sum all numbers under your : ' + sum);
}

function sumNumeralsInNumber(){
    //подсчитывает сумму цифр числа.
    let mNumber = Number( prompt("Please enter number:"));
    let sum = 0;
    while (mNumber>0){
        sum += mNumber % 10;
        mNumber = (mNumber - mNumber % 10) / 10;
    }
    alert('Sum all numbers under your : ' + sum);
}
function button4Factorial(){
    alert('Sum all numbers under your : ' + factorial( dialogWithUser() ) );
}

function button5TurnOver(){
    alert('Turn over number : ' + turnOver( dialogWithUser() ) );
}

function factorial(fn) {
//считает факториал числа.
    if (fn <= 1) return 1;
    return fn*factorial(fn-1)
}

function findMinimum( num1 = 0, num2 = 0, num3 = 0){
    let min = num1;
    if (num2 < min) { min = num2; }
    if (num3 < min) { min = num3; }
    alert('Minimum of ' + num1+', '+num2+', '+num3+' is ' + min );
    return min;
}
//рекурсия переворачивающая цифры в числе
function turnOver(N){
    if (N <= 0) { return 0;}
    return (N % 10)+( turnOver( (N - N % 10)/10 ) * 10)
}
//решение квадратного уравнения
function button6Quadratic(a, b, c){
    let D = b*b - 4*a*c, x1, x2;
    if (D<0) {return alert('There is no decision ');}
    else if (D == 0) {
        x1 = (-b + Math.sqrt(D) )/ 2*a;
        return alert('There is one decision: x1 = ' + x1 )
    }
    else {
        x1 = (-b + Math.sqrt(D) )/ 2*a;
        x2 = (-b - Math.sqrt(D) )/ 2*a;
        return alert('There is two decision: x1 = ' + x1 + ' and x2 = ' + x2);
    }
}

function dialogWithUser()
{
    let mNumber = Number( prompt("Please enter number:"));
    return mNumber;
}
