/*Напишите функцию, которая получает в качестве аргументов объект и строку и проверяет,
есть ли в этом объекте свойство с именем, равным этой строке.
 */
function testFun(objMy = {}, strMy = "") {
   if ( objMy[strMy]) return true;
    return false;
}
let Brest  = {
    name: "BREST",
    population: "340 318",
    born: 1017
}
console.log( testFun(Brest , "population") ? "YES":"NO");

/*Создайте объект для хранения своего любимого кулинарного рецепта.
Он должен содержать название, ингредиенты и их необходимое количество
(в виде вложенного объекта), а также количество порций (больше 1).
Напишите функцию, которая получает "рецепт", подсчитывает, сколько каждого
ингредиента нужно на одну порцию, и выводит информацию об этом в консоль.
 */
function howManyIngredientsForOneItem( InObj = {}){
    console.log(InObj.name + "/For one serving you will need: ")
    for ( let key in InObj.Ingredients){
        console.log( key + ":" + InObj.Ingredients[key]/InObj.portion);
    }
}
let PancakeRecipe  = {
    name: "Pancake",
    Ingredients: {
        milk:500,
        eggs: 3,
        flour: 200,
        butter: 30,
        salt: 3,
        sugar: 30
    },
    portion: 11
}
howManyIngredientsForOneItem(PancakeRecipe);

/*Создайте объект "Цилиндр" (свойства – радиус и высота).
Добавьте в него метод, который считает объём цилиндра (используя this).
 */
let Cylinder = {
    radius: 25,
    len: 12,
    area(){
        return Math.PI*(this.radius**2);
    },
    volume(){
        return Math.PI*(this.radius**2)*this.len;
    },
    lengthCircle(){
        return 2*Math.PI*this.radius;
    },
    printOptions(){
        console.log("Length of circle :" + this.lengthCircle());
        console.log("Volume of cylinder :" + this.volume());
        console.log("Area of circe :" + this.area());
    }
}
Cylinder.printOptions();

/*Выберите пингвина из списка вымышленных пингвинов на Википедии и опишите его в виде объекта
(не менее трёх полей; например, имя, создатель и источник). Добавьте этому объекту свойство canFly.
Добавьте два метода: sayHello, который выводит в консоль приветствие и представление вашего пингвина,
и fly, который в зависимости от значения свойства canFly (true или false) определяет,
может ли пингвин летать, и сообщает об этом в консоль.
 */
let penguinPororo = {
    name: "Pororo",
    creator : "South Korea",
    friends : {
        dino : "Krong",
        fox : "Eddi",
        bear : "Pobi"
    },
    canFly: false,
    sayHello(){
        console.log("I am penguin :" + this.name + ".");
        this.fly();
        console.log("My creator :" + this.creator);
        console.log("My friends :");
        for ( let key in this.friends){
            console.log( key +":" + this.friends[key]);
        }
    },
    fly(){
        this.canFly ? console.log("I can fly."):console.log("I can't fly.");
    }
}

penguinPororo.sayHello();