/*Создайте функцию, которая принимает три числа: два первых должны быть длиной сторон
катетов прямоугольного треугольника, а третье – длиной гипотенузы.
Функция возвращает true, если такой прямоугольный треугольник возможен, и false, если нет.
 */
let triangle = {
    lineA: 1,
    lineB: 1,
    lineC: 1,
    isThisTriangle() {
        if (this.lineA * this.lineB * this.lineC === 0) {
            return false;
        }
        this.sortLines();
        if (this.lineA + this.lineB < this.lineC) {
            return false;
        }
        return true;
    },
    isThisTriangle90(){
        this.sortLines();
        if (this.lineA**2 + this.lineB**2 === this.lineC**2){return true;}
        return  false;
    },
    sortLines(){
        if (this.lineA > this.lineB ) {
            let temp = this.lineA;
            this.lineA = this.lineB;
            this.lineB = temp;
        }
        if (this.lineB > this.lineC ) {
            let temp = this.lineB;
            this.lineB = this.lineC;
            this.lineC = temp;
        }
    },
    sayAlert(){
        this.isThisTriangle()? alert('This is triangle') :alert("This is not triangle");
        this.isThisTriangle90()? alert('This is triangle with angle 90') :alert("This triangle hasn't angle 90");
    },
    Triangle(lineA, lineB,lineC){
        this.lineA = lineA;
        this.lineB = lineB;
        this.lineC = lineC;
    }

}
triangle.Triangle(5, 3, 4);
triangle.sayAlert()

/*Создайте функцию repeat(str, n), которая возвращает строку,
состоящую из n повторений строки str. По умолчанию n=2, str — пустая строка.
 */
function repeat(str = '', n = 2) {
    let str1 = '';
    for (let i = 0; i < n; i++){
        str1 +=str;
    }
    return str1;
}
/*Создайте функцию, которая принимает два аргумента – количество учеников в классе и количество
парт в кабинете. Функция возвращает строку вида «не хватает 2 парт» / «1 лишняя парта».
 */

function places(students = 0, desks = 0) {
    students+=students % 2;
    let count = students / 2;
    if (count===desks) {alert('Desks enaugh for students'); return;}
    if (count>desks) {alert('we need' + (count-desks) +' desks'); return;}
    else {alert((desks-count) +' desks over'); return;}
}
places(5,3);
places(6,10);