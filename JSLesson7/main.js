/*Напишите функцию, которая создаёт и возвращает массив длиной 20 элементов,
каждый из которых – случайное число от 1 до 50.
 */
function randomRange50(range=20) {
    let arrRange = [];
    for (let i = 0  ; i<range;i++){
        arrRange[i] = ((Math.random()/2).toFixed(2)*100).toFixed();
    }
    return arrRange;
}
console.log(randomRange50(20).toString());

/*Перепишите функцию из задачи 1, так, чтобы она принимала три аргумента:
длину нужного массива, минимальное и максимальное значения элементов.
 */
function randomRange(range=20, min = 1 , max = 50 ) {
    let arrRange = [];
    if (min<1){min = 1};
    if ( max<min) return false;
    let temp = max, order = 1;
    /*высчитываем разрядность максимального числа*/
    while (temp > 0){
        order *=10;
        temp = (temp - (temp % 10))/10;
    }
    /*заполняем массив*/
    for (let i = 0  ; i<range;i++){
        temp = order;
        //находим число в заданном диапазоне
        while ( temp > max || temp<min){
            temp = (Math.random()*order).toFixed();
        }
        arrRange[i] = temp;
    }
    return arrRange;
}
console.log(randomRange(30, 2, 27).toString());

/*Напишите функцию, которая проверяет, начинается ли строка на https:// и
заканчивается ли она на .html. Если оба условия выполнены,
функция возвращает true, в ином случае – false.
 */

function isLink(linkStr = '') {
    if ( linkStr.length < 12 ) return false;
    linkStr.toLowerCase();
    if ( linkStr.indexOf("https://") != 0 )  return false;
    if ( linkStr.indexOf(".html", (linkStr.length -5)) == 0 ) return false;
    return true;
}
console.log(isLink("https://fd.htmlsaf.html").toString());
/*Напишите функцию, которая считает, сколько раз определённый символ встречается в строке.
*Перепишите функцию из задачи 4 так,
чтобы она считала большие и маленькие буквы одним и тем же символом (например, 'A' == 'a').
 */
function findCharIndexInStr(InStr = '', InChar =' ') {
    let count = 0;
    InStr = InStr.toLowerCase();
    InChar = InChar.toLowerCase();
    for ( let ch in InStr){
        if (InStr[ch] == InChar){
            count ++;
        }
    }
    return count;
}
console.log(findCharIndexInStr("aasdfffddVgbvsv",'V'));

/*Напишите функцию, которая выводит в консоль текущие дату, месяц и год в формате «24 января 2019».
 */
function dateNow() {
    let datN = new Date().toString();
    console.log(datN.slice(8,11)+datN.slice(4,8)+datN.slice(11,15));
}
dateNow();
//Напишите функцию, которая выводит в консоль количество секунд, прошедших с начала текущего дня.
function secondInDay() {
    let datN = new Date();
    console.log('Seconds = '+ (datN.getHours()*60*60+datN.getMinutes()*60+datN.getSeconds()));
}
secondInDay();
/*Напишите функцию, которая принимает массив случайных чисел (см. задачу 2 в практике) и
создаёт на его основе новый. Элементы нового массива – объекты вида
{initial: num1, sqrt: num2, floor: boolean1, ceil: boolean2}. initial –
значение элемента исходного массива с тем же индексом, sqrt – корень квадратный из этого значения.
Если округление sqrt по обычным математическим правилам сходится с его округлением через floor,
то floor = true, а ceil = false. Если сходится с округлением через ceil - наоборот. Например,
пусть у исходного массива arr[0] = 19, тогда в новом массиве newArr[0] =
{initial: 19, sqrt: 4.358898943540674, floor: true, ceil: false}.
 */
function ArrayObj(Number){ //конструктор обьекта
    this.initial = Number;
    this.sqrt = Math.sqrt( Number);
    if (Math.round(this.sqrt) == Math.floor(this.sqrt) ) {
        this.floor = true;
    }
        else this.floor = false;
    if (Math.round(this.sqrt) == Math.ceil(this.sqrt) ) {
        this.ceil = true;
    }
    else this.ceil = false;
}

function randomObj( InArr = []) {


    let newArray = [];
    for (let i in InArr) {
        newArray[i] = new ArrayObj(InArr[i]);
    }
    for (let i in newArray) {
        console.log(newArray[i]);
    }
}
randomObj( randomRange(10,2,27));