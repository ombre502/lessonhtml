function counterFactory(start = 0, step =1){
    function ticToc() {
        start+=step;
    return start;
    }
    return ticToc;
}
function take( tictoc, x = 1){
    let arr = [];
    for(let i=0; i<x; i++){
        arr[i] = tictoc() ;
    }
    return arr;
}

let FTicToc = counterFactory(3,2);
console.log( take(FTicToc, 10).toString() );

text = "Разбейте текст этой задачи на отдельные слова, удаляя по пути точки и запятые," +
    "а полученные слова сложите в массив. Напишите функцию, которая возвращает массив из тех же слов," +
    "но развёрнутых задом наперёд, причём массив должен быть отсортирован по количеству букв в слове." +
    "Напишите другую функцию, которая считает общее количество букв с во всех элементах массива.";
function splitText(text = ''){  //разделение текста на слова, возвращает массив из слов текста
    let arr = text.split(/[ ,;:\-"'.\n\t]+/);
    if ( arr[arr.length - 1] == '' ){  //проверка на последний пустой элемент массива
        arr.pop();
    }
    return arr;
}

// пустышка функция для возврата ссылки на реально нужную функцию подсчета вхождения символа в массив строк
function shellFunc(){
    let counter = 0;
    // делает подсчет заданного символа в массиве строк
    function countLetter(item){
        for(let key in item){
            if (item[key].toLowerCase() == 'с') {
                counter+=1;
            }
        }
    }
    // функция возвращающая значение счетчика
    function retCounter(){
        return counter;
    }
    return [countLetter, retCounter];
}

//получаем массив слов из текста
let arrText = splitText(text);
//сортируем массив на основании количества символов в строке
arrText.sort(function sortArr(a,b){
    return (a.length - b.length);
});
//переворот строки в массиве
arrText.forEach(function (item, index, ArrStr) {
    let arr = '';
    for (let i = item.length-1; i>=0; i--){
        arr+=item[i];
    }
    ArrStr[index] = arr;
}
)
let [FunCounterLetter, ViewCounter] = shellFunc();
let howMatchC = arrText.forEach( FunCounterLetter );
console.log( ViewCounter() );
console.log( arrText );
