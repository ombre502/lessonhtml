let varbl =  ""+ 1 + 0 ;
console.log( typeof (varbl), "= \"\"+ 1 + 0  = ", varbl);

varbl =  "" - 1 + 0 ;
console.log( typeof (varbl), "= \"\" - 1 + 0 = ", varbl);

varbl =  true + false ;
console.log( typeof (varbl), "= true + false = ", varbl);

varbl =  6 / "3" ;
console.log( typeof (varbl), "= 6 / \"3\" =", varbl);

varbl =  "2" * "3" ;
console.log( typeof (varbl), "= \"2\" * \"3\" = ", varbl);

varbl =  4 + 5 + "px" ;
console.log( typeof (varbl), "= 4 + 5 + \"px\" = ", varbl);

varbl =  "$" + 4 + 5 ;
console.log( typeof (varbl), "= \"$\" + 4 + 5 = ", varbl);

varbl =  "4" - 2 ;
console.log( typeof (varbl), "= \"4\" - 2 = ", varbl);

varbl =  "4px" - 2 ;
console.log( typeof (varbl), "= \"4px\" - 2 = ", varbl);

varbl =  7 / 0 ;
console.log( typeof (varbl), "= 7 / 0 = ", varbl);

varbl =  "  -9\n" + 5;
console.log( typeof (varbl), "= \"  -9\\n\" + 5 = ", varbl);

varbl =  "  -9\n" - 5;
console.log( typeof (varbl), "= \"  -9\\n\" - 5 = ", varbl);

varbl =  null + 1;
console.log( typeof (varbl), "= null + 1 = ", varbl);

varbl =  undefined + 1;
console.log( typeof (varbl), "= undefined + 1 = ", varbl);

varbl =  null == "\n0\n";
console.log( typeof (varbl), "= null == \"\\n0\\n\" = ", varbl);

let a = 25;
let b = 17;
console.log("start A =" , a);
console.log("start B =" , b);

console.log("with third variable c:");
c = a;
a = b;
b = c;

console.log("A =" , a);
console.log("B =" , b);

console.log("without c, math method:");
b=b+a;
a=b-a;
b=b-a;
console.log("A =" , a);
console.log("B =" , b);

console.log("without c, logical method XOR:");
a= a ^ b;
b= b ^ a;
a= a ^ b;

console.log("A =" , a);
console.log("B =" , b);
