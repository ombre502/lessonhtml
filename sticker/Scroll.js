window.addEventListener('scroll', function() {
    if ( document.documentElement.clientHeight*1.3 < pageYOffset ) {
        bground1.style.position = 'relative';
        bground2.style.display = 'block';
    }
    else{
        bground1.style.position = 'sticky';
        bground2.style.display = 'none';
    }
});